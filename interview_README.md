#instructions

1. clone this project
2. choose api to use (see APIs below)
3. decide what http solution to use
    - XMLHttpRequest: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
    - fetch: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
    - axios: https://github.com/axios/axios
    - other?
4.  

####APIs
- dogs: https://dog.ceo/dog-api/
- cats: https://documenter.getpostman.com/view/4016432/RWToRJCq#intro
