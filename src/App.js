import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    dogImg: '',
  };

  componentDidMount() {
    this.fetchImg();
  }

  fetchImg = () => {
    fetch(new Request('https://dog.ceo/api/breeds/image/random'))
        .then((res) => res.json())
        .then((json) => {
          this.setState({
            dogImg: json.message,
          });
        });
  };

  render() {
    return (
        <div className="App">
          <header className="App-header" onClick={this.fetchImg}>
            <img src={this.state.dogImg} className="App-logo" alt="logo" />
          </header>
        </div>
    );
  }
}

export default App;
